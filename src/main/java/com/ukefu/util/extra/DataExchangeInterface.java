package com.ukefu.util.extra;

import java.io.Serializable;

public interface DataExchangeInterface {
	
	
	public Serializable getDataByIdAndOrgi(String id, String orgi) ;
	
}
